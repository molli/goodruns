Fabricator(:run) do
  name Faker::Product.product_name
  organization Faker::Company.name
  service_link Faker::Internet.http_url
  date { r = Random.new(); r.rand(Time.now..7.days.from_now) }
  city Faker::Address.city
  state Faker::AddressUS.state_abbr
  address Faker::Address.street_address
  country Faker::Address.country
  mileage do |i|
    m = []
    3.times { r = Random.new(); m << r.rand(5..21) }
    m.join(', ')
  end
  description Faker::Lorem.paragraph(2)
  latitude Faker::Geolocation.lat
  longitude Faker::Geolocation.lng
  gmaps true
end
