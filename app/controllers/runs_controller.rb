class RunsController < ApplicationController
  expose(:run)

  def show
    @run_json = Gmaps4rails.build_markers([run]) do |user, marker|
      marker.lat user.latitude
      marker.lng user.longitude
    end.to_json
  end
end
