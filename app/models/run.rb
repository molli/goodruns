class Run < ActiveRecord::Base
  def location
    "#{city} (#{state}) - #{country}"
  end

  def title
    "#{name} - #{city} (#{state})"
  end

  # describe how to retrieve the address from your model
  def gmaps4rails_address
    s = ''
    s << "#{address}, " if address.present?
    s << "#{state} #{city}, #{country}"
  end

  private
    def run_params
      params.require(:run).permit(:name, :organization, :service_link, :date, :city, :state, :address, :country, :mileage,
                  :description, :latitude, :longitude, :gmaps)
    end
end
