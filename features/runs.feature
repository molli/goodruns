Feature: Runs

  Scenario: I can see the run info
    When I visit a run page
    Then I see its name
    And I can see who organize it
    And I can see a service link
    And I can see when it takes place
    And I can see its location
    And I can see its distances
    And I can see its description
