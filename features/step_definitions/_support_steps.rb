def run(options = {})
  unless @run
    if Run.last
      @run = Run.last
    else
      @run = Fabricate(:run, options)
    end
  end
  @run
end

def rand_time(from, to = Time.now)
  Time.at(rand_in_range(from.to_f, to.to_f))
end
