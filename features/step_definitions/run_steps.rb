When(/^I visit a run page$/) do
  @run = Fabricate(:run)
  visit run_path @run
end

Then(/^I see its name$/) do
  page.should have_content(@run.name)
end

Then(/^I can see who organize it$/) do
  page.should have_content("Organization: #{@run.organization}")
end

Then(/^I can see a service link$/) do
  page.should have_link("Link: #{@run.service_link}")
end

Then(/^I can see when it takes place$/) do
  page.should have_content("Date: #{@run.date.to_s(:short)}")
end

Then(/^I can see its location$/) do
  page.should have_content("Address: #{@run.address}")
  page.should have_content("Location: #{@run.city} (#{@run.state}) - #{@run.country}")
end

Then(/^I can see its distances$/) do
  page.should have_content("Mileage: #{@run.mileage}")
end

Then(/^I can see its description$/) do
  page.should have_content("Other info: #{@run.description}")
end
