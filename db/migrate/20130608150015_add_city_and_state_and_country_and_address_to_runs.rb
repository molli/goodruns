class AddCityAndStateAndCountryAndAddressToRuns < ActiveRecord::Migration
  def change
    add_column :runs, :city, :string
    add_column :runs, :state, :string
    add_column :runs, :country, :string
    add_column :runs, :address, :string
  end
end
