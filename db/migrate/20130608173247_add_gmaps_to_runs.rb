class AddGmapsToRuns < ActiveRecord::Migration
  def change
    add_column :runs, :gmaps, :boolean
  end
end
