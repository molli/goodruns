class AddLatitudeAndLongitudeToRuns < ActiveRecord::Migration
  def change
    add_column :runs, :latitude, :float
    add_column :runs, :longitude, :float
  end
end
