class AddServiceLinkToRuns < ActiveRecord::Migration
  def change
    add_column :runs, :service_link, :string
  end
end
