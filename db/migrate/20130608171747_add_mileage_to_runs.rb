class AddMileageToRuns < ActiveRecord::Migration
  def change
    add_column :runs, :mileage, :string
  end
end
