class AddOrganizationToRuns < ActiveRecord::Migration
  def change
    add_column :runs, :organization, :string
  end
end
