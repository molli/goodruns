# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Run.create name: '13a Marcialonga',
           organization: 'Road Runners Poviglio',
           service_link: 'www.roadrunnerspoviglio.it',
           date: 2.days.from_now,
           city: 'Poviglio',
           state: 'RE',
           address: 'via Parma 10',
           country: 'Italia',
           mileage: '5 - 10 - 21',
           description: 'Tiramisu sugar plum cake. Cheesecake lollipop jujubes gingerbread cupcake faworki applicake. Tootsie roll chocolate cake tootsie roll carrot cake gummies.
Sweet roll gummies sesame snaps gummi bears. Macaroon ice cream biscuit. Pie cotton candy soufflé fruitcake chocolate lollipop. Jelly-o marzipan croissant chocolate bar.',
           latitude: 44.842193,
           longitude: 10.538571,
           gmaps: true
